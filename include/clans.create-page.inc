<?php

function clans_create_clan() {
  global $user; // Use the Info of current user who is visiting /create
  // if visiter isnt online user send them to /user page where they can login
  if($user->uid == 0) {
    drupal_goto('user');
  }

  // Select the clan of the current visiter
  $clan = db_select('clans', 'c')
    ->fields('c')
    ->condition('cid', $user->cid,'=')
    ->execute()
    ->fetchAssoc();

  // If user is in clan 1 the may create a clan (Clan ID 1 == None clan)
  if ($clan['cid'] == 1) {
    return drupal_get_form('clans_create_clan_form'); // return form
  // If users isnt in clan 1 they are already in a clan
  } else {
    // Set a message that they're already in a clan
    $content[] = array(
      '#type' => 'markup',
      '#markup' => t('You\' re already in @clan, if you want to create a clan you need to leave @clan first.', array('@clan' => $clan['name'])),
    );
    return $content; // Returning message
  }
}

/**
 * Form for creating clans
 */
function clans_create_clan_form($form_state) {
  $form = array(); // Create form array that holds field values

  // Clan name textfield
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Clan Name'),
    '#required' => TRUE,
    '#maxlength' => 20,
    '#size' => 40,
  );
  // Clan tag textfield
  $form['tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Clan Tag'),
    '#description' => T('Clan tag of 4 characters'),
    '#required' => TRUE,
    '#size' => 6,
    '#maxlength' => 4,
  );
  // Clan description textarea
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Clan Description'),
    '#default_value' => t('This is a Clan Description!'),
  );

  // Clan create submit button
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Clan!'),
  );

  return $form;
}

/**
 * Validate clans_create_clan_form
 */
function clans_create_clan_form_validate(&$form, &$form_state) {
  // If clan name is shorter then 4 send form error
  if (strlen($form_state['values']['name']) < 4) {
    // Create form error for textfield 'name'
    form_set_error('name', t('Clan Name need at least 4 characters.'));
  } else {
    // Query to check if clan name is already in use
    $clan = db_select('clans', 'c')
      ->fields('c')
      ->condition('name', $form_state['values']['name'],'!=')
      ->condition('name', $form_state['values']['name'],'=')
      ->execute()
      ->rowCount();

    $forBiddenName = $form_state['values']['name'] == 'edit';
    $forBiddenNames = $form_state['values']['name'] == 'leave';

    // If $clan not outputs 0 then the clan name is already in use and we need to create a error
    if ($clan != 0) {
      // Create form error for textfield 'name'
      form_set_error('name', t('Clan Name already in use.'));
    } else if ($forBiddenName || $forBiddenNames) {
      form_set_error('name', t('Clan Name may not be used.'));
    }
  }

  // Query to check if clan tag is already in user
  $tag = db_select('clans', 'c')
    ->fields('c')
    ->condition('tag', $form_state['values']['tag'],'=')
    ->execute()
    ->rowCount();

  // If $tag not outputs 0 and textfield 'tag' is not empty: create a error
  if ($tag != 0 && $form_state['values']['tag']) {
    // Create form error for textfield 'tag'
    form_set_error('tag', t('Clan Tag already in use.'));
  }
}

/**
 * Submit form clans_create_clan_form
 */
function clans_create_clan_form_submit(&$form, &$form_state) {
  global $user; // Get $user info of user who submitted form
  $clan = $form_state['values']; // Change $form_state['values'] to $clan (looks better)

  // Insert clan info to Database
  $query = db_insert('clans')
    ->fields(array(
      'name' => $clan['name'],
      'tag' => $clan['tag'],
      'description' => $clan['description'],
      'created_at' => time(),
      'creator' => $user->name,
    ))
    ->execute();

  // Select cid (Clan ID) of the new clan
  $cid = db_select('clans', 'c')
    ->fields('c')
    ->condition('name', $clan['name'],'=')
    ->execute()
    ->fetchAssoc();

  // Set the users current cid to his new clan cid and set his rank to 3 (Rank 3 == Leader)
  $query = db_update('users')
    ->fields(array(
      'cid' => $cid['cid'],
      'rank' => 3,
    ))
    ->condition('uid', $user->uid, '=')
    ->execute();

  // Set a message that users clan has created
  drupal_set_message(t('The Clan: @name has been created.', array('@name' => $clan['name'])));
  // Set redirect value to clan, so he see his new clan
  $form_state['redirect'] = 'clan';
}
