<?php

/**
 * Page callback for /clan/leave
 */
function clan_leave() {
  global $user;
  if ($user->cid == 1) {
    drupal_set_message(t("Your not in a clan."), 'warning', FALSE);
    drupal_goto('user');
  }
  drupal_set_title('Are you sure to leave your clan?');
  return drupal_get_form('clan_leave_form');
}

/**
 * Create form: clan_leave_form
 */
function clan_leave_form() {
  $form = array();

  $form['buttons']['yes'] = array(
    '#type' => 'submit',
    '#value' => t('Yes'),
  );
  $form['buttons']['no'] = array(
    '#type' => 'submit',
    '#value' => t('No'),
  );

  return $form;
}

/**
 * Validate if user can leave the clan
 */
function clan_leave_form_validate(&$form, &$form_state) {
  if ($form_state['triggering_element']['#value'] == "No") {
    drupal_goto('clan');
  } else {
    global $user;
    $clan = db_select('clans', 'c')
      ->fields('c')
      ->condition('cid', $user->cid ,'=')
      ->execute()
      ->fetchAssoc();

    if ($user->rank == 3) {
      _leave_rank_3();
    } else {
      // Query that add user to none clan
      $query = db_update('users')
        ->fields(array(
          'cid' => 1,
          'rank' => 1,
        ))
        ->condition('uid', $user->uid, '=')
        ->execute();

      drupal_set_message(t("You leaved your clan."), 'status', FALSE);
      drupal_goto('user');
    }
  }
}

/**
 * Function that checks if the leader is the only member of the clan.
 * If he is we need to delete the clan
 * If he isn't we need to say he can't leave his clan
 */
function _leave_rank_3() {
  global $user;
  $members = db_select('users', 'u')
    ->fields('u')
    ->condition('cid', $user->cid , '=')
    ->condition('uid', $user->uid , '!=')
    ->execute()
    ->rowCount();

  if ($members == 0) {
    // Query that deletes clan
    $delete = db_delete('clans')
      ->condition('cid', $user->cid)
      ->execute();

    // Query that add user to none clan
    $query = db_update('users')
      ->fields(array(
        'cid' => 1,
        'rank' => 1,
      ))
      ->condition('uid', $user->uid, '=')
      ->execute();

    drupal_set_message(t("Clan is deleted."), 'status', FALSE);
  } else {
    drupal_set_message(t("You 're a Clan Leader, you can't leave the clan because there are members in your clan."), 'warning', FALSE);
  }
}
