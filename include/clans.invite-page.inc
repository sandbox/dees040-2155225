<?php

/**
 * Page callback for /user/%/invite
 */
function clans_invite_player($player) {
  // Check if player exists
  if(is_numeric($player) && ($account = user_load($player))) {
    global $user; // Use current user to see his rank and Clan id
    // Make a Query to check if clan already has there max players
    $ClanMaxMembers = db_select('clans', 'c')
      ->fields('c')
      ->condition('cid', $user->cid, '=')
      ->execute()
      ->fetchAssoc();

    // Count all members from the inviters clan
    $numOfMembers = db_select('users', 'u')
      ->fields('u')
      ->condition('cid', $user->cid, '=')
      ->condition('rank', 1, '=')
      ->execute()
      ->rowCount();

    if ($ClanMaxMembers['members'] == $numOfMembers) {
      // Create warning message and send current page viewer to his clan profile
      drupal_set_message(t("Your clan already has there max members"), 'warning', FALSE);
      drupal_goto('clan');
    } else {
      // Make a Query to check of invited player is already in a clan
      $isClanMember = db_select('users', 'u')
        ->fields('u')
        ->condition('uid', $account->uid, '=')
        ->condition('cid', 1, '!=')
        ->execute()
        ->rowCount();

      // If player already is in a clan create an warning message
      if ($isClanMember) {
        // Create warning message and send current page viewer to $player profile
        drupal_set_message(t("@name is already in a clan.", array('@name' => $account->name)), 'warning', FALSE);
        drupal_goto('user/'.$player);

      } else {
        // Create query to check of clan already has invited player
        $alreadyInvited = db_select('clans_invite', 'i')
          ->fields('i')
          ->condition('cid', $user->cid, '=')
          ->condition('uid', $account->uid, '=')
          ->execute()
          ->rowCount();

        // Check of clan already has invited player
        if ($alreadyInvited) {
          // Create warning message and send current page viewer to $player profile
          drupal_set_message(t("@name is already invited for your clan.", array('@name' => $account->name)), 'warning', FALSE);
          drupal_goto('user/'.$player);
        } else {
          // If user isn't a Leader or Admin he may not invite Players
          if ($user->rank != 1) {
            // Create Database invite
            $query = db_insert('clans_invite')
              ->fields(array(
                'type' => 0, //0 = Clan invites Player, 1 = Player invites Clan.
                'uid' => $account->uid,
                'cid' => $user->cid,
              ))
              ->execute();

            // Player has been invited with succes, create a message and send current page viewer to player profile
            drupal_set_message(t('@name is invited for your clan.', array('@name' => $account->name)), 'status', FALSE);
            drupal_goto('user/'.$player);

          } else {
            // User isnt a admin or leader so send him to his own profile and set an error message
            drupal_set_message(t('You may not invite Players.'), 'error', FALSE);
            drupal_goto('user/'.$user->name);
          }
        }
      }
    }
  } else {
    // If player doesn't exist return drupal_not_found page
    return drupal_not_found();
  }
}
