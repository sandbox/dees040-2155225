<?php

/**
 * @file
 * This file we update the clan score
 * Every user has his own score, when going to this page foreach clan al the members there score will set the clan score
 */
function updateScore() {
  // Select all clans
  $clans = db_select('clans', 'c')
    ->fields('c')
    ->condition('cid', 1, '!=')
    ->execute()
    ->fetchAllAssoc('cid');

  foreach ($clans as $clan) {
    $users = db_select('users', 'u')
      ->fields('u')
      ->condition('cid', $clan->cid, '=')
      ->condition('uid', 0, '!=')
      ->execute()
      ->fetchAllAssoc('uid');

    $score = 0;

    foreach ($users as $user) {
      $score += $user->score;
    }

    $query = db_update('clans')
      ->fields(array(
        'score' => $score,
      ))
      ->condition('cid', $clan->cid, '=')
      ->execute();

    $score = 0;

  }

  drupal_set_message(t('Score updated!'), 'status', FALSE);
  drupal_goto('');

}
