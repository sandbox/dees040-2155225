<?php

/**
 * Implenets hook_theme()
 */
function clans_theme() {
  // Define 2 templates file. One for the 'None' clan when the users isnt in a clan and one for the user current clan
  return array(
    // Existing clan template
    'clans_item' => array(
      'arguments' => array('name' => NULL, 'tag' => NULL, 'score' => NULL, 'description' => NULL, 'created_at' => NULL, 'creator' => NULL),
      'template' => 'template/clans-page',
    ),
    // None existing clan
    'clans_none' => array(
      'arguments' => array('title' => NULL),
      'template' => 'template/clans-none',
    ),
  );
}

/**
 * Page callback for /clan
 */
function clans_page() {
  global $user; // Use $user info of current user who is viewing /clan

  // if visiter isnt online user send them to /user page where they can login
  if($user->uid == 0) {
    drupal_goto('user');
  } else if($user->cid == 1) {

    $content[] = array(
      '#type' => 'markup',
      '#markup' => theme('clans_none', array('title' => t('You are not in a clan'))),
    );

  } else {

    // Select the clan of the current user
    $clan = db_select('clans', 'c')
      ->fields('c')
      ->condition('cid', $user->cid,'=')
      ->execute()
      ->fetchAssoc();

    $content = _create_page($clan); // Send clan info to _create_page
  }

  return $content;
}

/**
 * Page callback for /clan/%
 */
function clans_page_get($clan) {
  $clan_info;
  if(is_numeric($clan)) {
    $clan_info = db_select('clans', 'c')
      ->fields('c')
      ->condition('cid', $clan,'=')
      ->execute();

    if ($clan_info->rowCount() != 0) {
      $clan_info = $clan_info->fetchAssoc();
    } else {
      drupal_set_message(t('Clan doesn\'t exists'), 'error', FALSE);
      drupal_goto('user');
    }

  } else {
    $clan_info = db_select('clans', 'c')
      ->fields('c')
      ->condition('name', $clan,'=')
      ->execute();

    if ($clan_info->rowCount() != 0) {
      $clan_info = $clan_info->fetchAssoc();
    } else {
      drupal_set_message(t('Clan doesn\'t exists'), 'error', FALSE);
      drupal_goto('user');
    }
  }

  return _create_page($clan_info); // Send clan info to _create_page and return it
}

/**
 * Function which sets clan info into clan page
 */
function _create_page($clan) {
  drupal_set_title($clan['name']); // Set the title of page to Clan Name

  // Send clan info to existing clan template
  $content[] = array(
    '#type' => 'markup',
    '#markup' => theme('clans_item', _clans_page_item($clan)),
  );

  return $content; // Return content info
}

/**
 * Check al info which is send to template to be plain text before sending
 */
function clans_preprocess_clans_item(&$variables) {
  $variables['name'] = check_plain($variables['name']);
  $variables['tag'] = check_plain($variables['tag']);
  $variables['score'] = check_plain($variables['score']);
  $variables['description'] = check_plain($variables['description']);
  $variables['created_at'] = check_plain($variables['created_at']);
  $variables['creator'] = check_plain($variables['creator']);
  $variables['members_n'] = check_plain($variables['members_n']);
}

/**
 * Function to get a clan info we need
 */
function _clans_page_item($clan) {
  // Select all users who are in the current viewing clan
  $leaders = db_select('users', 'u')
    ->fields('u')
    ->condition('cid', $clan['cid'],'=');
  $admins = db_select('users', 'u')
    ->fields('u')
    ->condition('cid', $clan['cid'],'=');
  $members = db_select('users', 'u')
    ->fields('u')
    ->condition('cid', $clan['cid'],'=');
  $count = db_select('users', 'u')
    ->fields('u')
    ->condition('cid', $clan['cid'],'=');

  $members_array = array(); // Create array for holding all the clan members, admins and leaders

  // Get a leaders from current clan and store them in members array
  $members_array['leaders'] = _clans_get_members(3, $clan['cid'], $leaders);
  // Get a admins from current clan and store them in members array
  $members_array['admins'] = _clans_get_members(2, $clan['cid'], $admins);
  // Get a members from current clan and store them in members array
  $members_array['members'] = _clans_get_members(1, $clan['cid'], $members);

  // Check if current user is a Clan Leader
  global $user;

  $link = "";
  if ($user->uid) {
    if (($user->rank == 3 || $user->rank == 2) && $user->cid == $clan['cid']) {
      // Add a Edit Clan link
      $link = "<a href='clan/edit'>Edit Clan</a>";
    } else if ($user->cid != $clan['cid']) {
      $link = "<a href='".$clan['name']."/join'>Join this clan</a>";
    }
  } else {
    $link = "Login to join this clan!";
  }

  // Store all clan info in information array and return it
  $information = array(
    'name' => $clan['name'],
    'tag' => $clan['tag'],
    'score' => $clan['score'],
    'description' => $clan['description'],
    'created_at' => $clan['created_at'],
    'creator' => $clan['creator'],
    'members_n' => $count->execute()->rowCount(),
    'leaders' => $members_array['leaders'],
    'admins' => $members_array['admins'],
    'members' => $members_array['members'],
    'link' => $link,
  );
  return $information;
}

/**
 * Function that return all users of clan by specified rank
 */
function _clans_get_members($rank, $cid, $query) {
  // Select all clan members by rank
  $people = $query->condition('rank', $rank, '=')->execute();
  // If $people isnt eqaul to 0 what means that there're
  if ($people->rowCount() != 0) {
    $people = $people->fetchAllAssoc('name');
    $people_array = array();

    foreach ($people as $i) { $people_array[] = $i->name; }

  } else {
    $people_array[] = 'None';
  }

  return $people_array;
}
