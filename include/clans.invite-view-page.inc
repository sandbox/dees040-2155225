<?php

/**
 * Page callback for /invites
 */
function clans_view_invite_clan() {
  return drupal_get_form('clans_invites_form');
}

function clans_invites_form() {
  $form = array(); // Create form array that holds field values

  $invites = _get_clan_invites(); // Call _get_clan_invites() to get all clan invites of current viewer

  // Clan invites select box
  $form['clan_member'] = array(
    '#type' => 'select',
    '#title' => t('Clan invites'),
    '#options' => $invites,
    '#empty_value' => 'None',
  );

  // Create accept clan button
  $form['buttons']['accept'] = array(
    '#type' => 'submit',
    '#value' => t('Accept Clan!'),
    '#submit' => array('clans_accept_submit'),
  );

  // Create decline clan button
  $form['buttons']['decline'] = array(
    '#type' => 'submit',
    '#value' => t('Decline Clan!'),
    '#submit' => array('clans_decline_submit'),
  );

  return $form;
}

/**
 * Function for accepting clan invite
 */
function clans_accept_submit(&$form, &$form_state) {
  global $user;

  // If none invites where selected return false so update code won't get execture
  if ($form_state['values']['clan_member'] == 'None') {
    return false;
  }

  // Query that add user to clan
  $query = db_update('users')
    ->fields(array(
      'cid' => $form_state['values']['clan_member'],
    ))
    ->condition('uid', $user->uid, '=')
    ->execute();

  // Query that deletes clan invite
  $delete = db_delete('clans_invite')
    ->condition('cid', $form_state['values']['clan_member'])
    ->condition('uid', $user->uid)
    ->execute();

  drupal_set_message(t('Clan invite accepted'), 'status', FALSE);
  drupal_goto('clan');


}

/**
 * Function for declining clan invite
 */
function clans_decline_submit(&$form, &$form_state) {
  global $user;

  // Query that deletes clan invite
  $delete = db_delete('clans_invite')
    ->condition('cid', $form_state['values']['clan_member'])
    ->condition('uid', $user->uid)
    ->execute();

  drupal_set_message(t('Clan invite declined'), 'status', FALSE);
}

/**
 * Function to get all the clan invites of current viewer
 */
function _get_clan_invites() {
  global $user;
  $invites_array = array(); // Create array that hols clan invites

  // Check if user is in a clan
  if ($user->cid == 1) { // If user is not in a clan get all clan invites
    $invites = db_select('clans_invite', 'i')
      ->fields('i')
      ->condition('uid', $user->uid ,'=')
      ->condition('type', 0, '=') //0 = Clan invites Player, 1 = Player invites Clan.
      ->execute();

    // Check if user has clan invites
    if ($invites->rowCount() != 0) {
      $invites = $invites->fetchAllAssoc('cid'); // Get all clan invites bij clan id

      // Foreach clan  invite get the clan name and id
      foreach ($invites as $cid) {
        $name = db_select('clans', 'c')
          ->fields('c')
          ->condition('cid', $cid->cid ,'=')
          ->execute()
          ->fetchAssoc();

        $invites_array[$name['cid']] = $name['name']; // Set the clan invite in the invites array
      }

    } else { // If user has none clan invites create a warning
      drupal_set_message(t("None clan invites"), 'warning', FALSE);
    }
  } else { // User is already in a clan. Create an error and send them to their clan page
    drupal_set_message(t('Your already in a clan.'), 'error', FALSE);
    drupal_goto('clan');
  }

  return $invites_array;
}
