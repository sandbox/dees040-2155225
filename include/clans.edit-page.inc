<?php

/**
 * Page callback for /clan/edit
 */
function clan_edit_page() {
  global $user;
  if ($user->cid == 1) {
    drupal_set_message(t('You\'re not permitted to edit this clan'), 'error', FALSE);
    drupal_goto('user');
  }

  drupal_set_title("Edit Clan");
  $forms = array();
  $forms[] = drupal_get_form('clans_edit_clan_form');
  $forms[] = drupal_get_form('clans_edit_members_form');
  $forms[] = drupal_get_form('clans_user_invite_form');
  if ($user->rank == 3) { $forms[] = drupal_get_form('clans_private_clan_form'); }
  return $forms; // return form
}

/**
 * Edit clan form
 */

/**
 * Create form: clans_edit_clan_form
 */
function clans_edit_clan_form() {
  // Select the clan of the current visiter
  $clan = _clans_get_info('clans', '=')
            ->execute()
            ->fetchAssoc();

  $form = array(); // Create form array that holds field values

  // Clan name textfield
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Clan Name'),
    '#description' => T('Only editable for Clan Leaders'),
    '#default_value' => t($clan['name']),
    '#maxlength' => 20,
    '#size' => 40,
  );
  // Clan tag textfield
  $form['tag'] = array(
    '#type' => 'textfield',
    '#title' => t('Clan Tag'),
    '#description' => T('Clan tag of 4 characters, only editable for Clan Leaders'),
    '#default_value' => t($clan['tag']),
    '#size' => 6,
    '#maxlength' => 4,
  );
  // Clan description textarea
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Clan Description'),
    '#default_value' => t($clan['description']),
  );

  // Clan create submit button
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Clan!'),
  );

  return $form;
}


/**
 * Validate clans_create_clan_form
 */
function clans_edit_clan_form_validate(&$form, &$form_state) {
  // If clan name is shorter then 4 send form error
  if (strlen($form_state['values']['name']) < 4) {
    // Create form error for textfield 'name'
    form_set_error('name', t('Clan Name need at least 4 characters.'));
  } else if (!preg_match('/^[a-z0-9 [\-\]\_]+$/i', $form_state['values']['name'])) {
    form_set_error('name', t('Clan Name only may have: A-Z, a-z, 0-9, ], [, -, or _'));
  } else {
    // Query to check if clan name is already in use
    $clan = _clans_get_info('clans', '!=')
            ->condition('name', $form_state['values']['name'],'=')
            ->execute()
            ->rowCount();

    $forBiddenName = $form_state['values']['name'] == 'edit';
    $forBiddenNames = $form_state['values']['name'] == 'leave';

    // If $clan not outputs 0 then the clan name is already in use and we need to create a error
    if ($clan != 0) {
      // Create form error for textfield 'name'
      form_set_error('name', t('Clan Name already in use.'));
    } else if ($forBiddenName || $forBiddenNames) {
      form_set_error('name', t('Clan Name may not be used.'));
    }
  }

  // Query to check if clan tag is already in user
  $tag = _clans_get_info('clans', '!=')
            ->condition('tag', $form_state['values']['tag'],'=')
            ->execute()
            ->rowCount();
  // If $tag not outputs 0 and textfield 'tag' is not empty: create a error
  if ($tag != 0 && $form_state['values']['tag']) {
    // Create form error for textfield 'tag'
    form_set_error('tag', t('Clan Tag already in use.'));
  }
}
/**
 * Submit form clans_create_clan_form
 */
function clans_edit_clan_form_submit(&$form, &$form_state) {
  global $user;

  $clan = $form_state['values']; // Change $form_state['values'] to $clan (looks better)

  // Update clan info to Database
  if ($user->rank == 3) { // If user rank equal to 3 the may update the clan name, tag and description
    $query = db_update('clans')
              ->fields(array(
                'name' => $clan['name'],
                'tag' => $clan['tag'],
                'description' => $clan['description'],
              ))
              ->condition('cid', $user->cid, '=')
              ->execute();
  } else { // If user rank is not equal to 3 the may only update the clan description
    $query = db_update('clans')
              ->fields(array(
                'description' => $clan['description'],
              ))
              ->condition('cid', $user->cid, '=')
              ->execute();
  }
  // Set a message that users clan has created
  drupal_set_message(t('The Clan: @name has been edited.', array('@name' => $clan['name'])));
  // Set redirect value to clan, so he see his new clan
  $form_state['redirect'] = 'clan';
}


/**
 * Edit members form
 */


/**
 * Create form: clans_edit_clan_form
 */
function clans_edit_members_form() {
  global $user;

  // Select the clan of the current visiter
  $query = _clans_get_info('users', '=');
  $query1 = _clans_get_info('users', '=');
  $query2 = _clans_get_info('users', '=');
  $query3 = _clans_get_info('users', '=');

  $admins = _get_clan_members_by_rank(2, $query);
  $leaders_current = _get_clan_members_by_rank(3, $query1);
  $members = _get_clan_members_by_rank(1, $query2);
  $admins_current = _get_clan_members_by_rank(2, $query3);

  $form = array(); // Create form array that holds field values

  // If user rank equal to 3 the may update all member ranks
  if ($user->rank == 3) {

    // Clan (new) Leaders select box
    $form['clan_leader'] = array(
      '#type' => 'select',
      '#title' => t('New Clan Leader'),
      '#options' => $admins,
      '#empty_value' => 'None',
    );

    // Clan (remove) Leaders select box
    $form['clan_leader_remove'] = array(
      '#type' => 'select',
      '#title' => t('Remove Clan Leader'),
      '#options' => $leaders_current,
      '#empty_value' => 'None',
    );

    // Clan (new) Admins select box
    $form['clan_admin'] = array(
      '#type' => 'select',
      '#title' => t('New Clan Admin'),
      '#options' => $members,
      '#empty_value' => 'None',
    );

    // Clan (remove) Admins select box
    $form['clan_admin_remove'] = array(
      '#type' => 'select',
      '#title' => t('Remove Clan Admin'),
      '#options' => $admins_current,
      '#empty_value' => 'None',
    );

  }

  // Clan (remove) Members select box
  $form['clan_member_remove'] = array(
    '#type' => 'select',
    '#title' => t('Remove Clan Member'),
    '#options' => $members,
    '#empty_value' => 'None',
  );

  // Clan create submit button
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Members!'),
  );

  return $form;
}

/**
 * Validate clans_edit_members_form
 */
function clans_edit_members_form_validate(&$form, &$form_state) {
  global $user;

  // If user rank equal to 3 the may update all member ranks, if its 3 we gone check some things
  if ($user->rank == 3) {

    $clan = $form_state['values']; // Change $form_state['values'] to $clan (looks better)

    // If select field 'clan_leader' isn't equal to 'none' check the next things
    if ($clan['clan_leader'] != 'None') {
      // Get all users of the clan with rank 3
      $leaders = _clans_get_info('users', '=')
        ->condition('rank', 3, '=')
        ->execute()
        ->rowCount();

      // Get the clan max ammount of leader(s)
      $leadersAllowd = _clans_get_info('clans', '=')
        ->execute()
        ->fetchAssoc();

      // Check if the clan already has there max number of leaders
      if ($leaders == $leadersAllowd['leaders']) {
        // If the already have the max amount of leaders set a form error
        form_set_error('clan_leader', 'You already have the maximum clan Leaders in your clan.');
      }
    }

    // If select field 'clan_leader_remove' isn't equal to 'none' check the next things
    if ($clan['clan_leader_remove'] != 'None') {
      global $user;
      // Check if the current users isn't trying to change there own leader status
      if ($clan['clan_leader_remove'] == $user->name) {
        // If the do, create a form error
        form_set_error('clan_leader_remove', t('You can\'t remove yourself'));
      }
    }

    // If select field 'clan_admin' isn't equal to 'none' check the next things
    if ($clan['clan_admin'] != 'None') {
      // Get all users of the clan with rank 2
      $admins = _clans_get_info('users', '=')
        ->condition('rank', 2, '=')
        ->execute()
        ->rowCount();

      // Get the clan max ammount of admin(s)
      $adminsAllowd = _clans_get_info('clans', '=')
        ->execute()
        ->fetchAssoc();

      // Check if the clan already has there max ammount of admin(s)
      if ($admins == $adminsAllowd['admins']) {
        // If the already have the max amount of admin(s) set a form error
        form_set_error('clan_admin', 'You already have the maximum clan Admins in your clan.');
      }
    }

    // If select field 'clan_admin_remove' isn't equal to 'none',
    // check the next things
    if ($clan['clan_admin_remove'] != 'None') {
      global $user;
      // Check if the current users isn't trying to change there own admin status
      if ($clan['clan_admin_remove'] == $user->name) {
        // If the do, create a form error
        form_set_error('clan_admin_remove', t('You can\'t remove yourself'));
      }
    }
  }

}

/**
 * Submit form clans_edit_members_form
 */
function clans_edit_members_form_submit(&$form, &$form_state) {
  global $user;
  $clan = $form_state['values']; // Change $form_state['values'] to $clan (looks better)

  // If user rank equal to 3 the may update all member ranks
  if ($user->rank == 3) {

    // If input field clan_leader is equal to 'None' send new info the update function
    if($clan['clan_leader'] != 'None') {
      _update_clan_members('rank', 3, $clan['clan_leader'], 'New clan Leader');
    }

    // If input field clan_leader_remove is equal to 'None' send new info the update function
    if ($clan['clan_leader_remove'] != 'None') {
      _update_clan_members('rank', 2, $clan['clan_leader_remove'], 'Clan Leader removed');
    }

    // If input field clan_admin is equal to 'None' send new info the update function
    if($clan['clan_admin'] != 'None') {
      _update_clan_members('rank', 2, $clan['clan_admin'], 'New Clan Admin');
    }

    // If input field clan_admin_remove is equal to 'None' send new info the update function
    if ($clan['clan_admin_remove'] != 'None') {
      _update_clan_members('rank', 1, $clan['clan_admin_remove'], 'Clan Admin removed');
    }

  }

  // If input field clan_member_remove is equal to 'None' send new info the update function
  if ($clan['clan_member_remove'] != 'None') {
    _update_clan_members('cid', 1, $clan['clan_member_remove'], 'Clan Member removed');
  }

}

/**
 * Accept or Decline user form
 */

/**
 * Create form: clans_user_invite_form
 */
function clans_user_invite_form() {
  $form = array(); // Create form array that holds field values

  // Call _get_clan_invites() to get all clan invites of current viewer
  $invites = _get_clan_invites();

  // Clan invites select box
  $form['clan_member'] = array(
    '#type' => 'select',
    '#title' => t('Clan invites'),
    '#options' => $invites,
    '#empty_value' => 'None',
  );

  // Create accept clan button
  $form['buttons']['accept'] = array(
    '#type' => 'submit',
    '#value' => t('Accept Player!'),
    '#submit' => array('clans_accept_submit'),
  );

  // Create decline clan button
  $form['buttons']['decline'] = array(
    '#type' => 'submit',
    '#value' => t('Decline Player!'),
    '#submit' => array('clans_decline_submit'),
  );

  return $form;
}

/**
 * Function for accepting clan invite
 */
function clans_accept_submit(&$form, &$form_state) {
  global $user;

  // If none invites where selected return false so update code won't get execture
  if ($form_state['values']['clan_member'] == 'None') {
    return false;
  }

  // Query that add user to clan
  $query = db_update('users')
    ->fields(array(
      'cid' => $user->cid,
    ))
    ->condition('uid', $form_state['values']['clan_member'], '=')
    ->execute();

  // Query that deletes clan invite
  $delete = db_delete('clans_invite')
    ->condition('cid', $user->cid)
    ->condition('uid', $form_state['values']['clan_member'])
    ->execute();

  // Let the clan admin/leader know that the player has been accepted
  drupal_set_message(t('Player invite accepted'), 'status', FALSE);
}

/**
 * Function for declining clan invite
 */
function clans_decline_submit(&$form, &$form_state) {
  global $user;

  // Query that deletes clan invite
  $delete = db_delete('clans_invite')
    ->condition('cid', $user->cid)
    ->condition('uid', $form_state['values']['clan_member'])
    ->execute();

  // Let the clan admin/leader know that the player has been declined
  drupal_set_message(t('Player invite declined'), 'status', FALSE);
}

/**
 * Private clan form
 */

/**
 * Create form: clans_private_clan_form
 */
function clans_private_clan_form() {
  global $user; // Get global user for Clan ID

  // Query for getting current clan status
  $status = _clans_get_info('clans', '=')
    ->execute()
    ->fetchAssoc();


  $form = array(); // Create array that holds form fields

  // Create radios input field
  $form['private_clan'] = array(
    '#type' =>  'radios',
    '#title' => t('Private clan'),
    '#default_value' => $status['private'],
    '#options' => array(
      0 => t('Private'),
      1 => t('Open'),
    ),
    '#description' => t('When a clan is private players can\'t join the Clan, only the clan can invite players.'),
  );

  // Clan create submit button
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update Status!'),
  );

  return $form; // Return form
}


/**
 * Submit form clans_edit_members_form
 */
function clans_private_clan_form_submit(&$form, &$form_state) {
  global $user;
  $clan = $form_state['values']; // Change $form_state['values'] to $clan (looks better)

  // Query that updates private clan status
  $query = db_update('clans')
    ->fields(array(
      'private' => $clan['private_clan'],
    ))
    ->condition('cid', $user->cid, '=')
    ->execute();

  // Create message thats let the user know the private clan status
  $status = "";
  if ($clan['private_clan']) {
    $status = "Open";
  } else {
    $status = "Private";
  }

  drupal_set_message(t('Clan is now: @status', array('@status' => $status)), 'status', FALSE);
}

/**
 * Function for getting query's, so we dont need to make a new query every function
 */
function _clans_get_info($clanOrUser, $equalTo) {
  global $user;
  return db_select($clanOrUser, 'q')
    ->fields('q')
    ->condition('cid', $user->cid, $equalTo);
}

/**
 * Function to get all clan members by there rank
 */
function _get_clan_members_by_rank($rank, $query) {
  // Select all clan members by rank
  $people = $query->condition('rank', $rank, '=')
    ->execute();

  $people_array = array();

  // If $people isnt equal to 0 what means that there're
  if ($people->rowCount() != 0) {
    $people = $people->fetchAllAssoc('name');

    foreach ($people as $i) {
      $people_array[$i->name] = $i->name;
    }

  }

  return $people_array;
}

/**
 * Function that updates new info to database and set's a message
 */
function _update_clan_members($field, $value, $condition, $message) {
  $query = db_update('users')
    ->fields(array(
      $field => $value,
    ))
    ->condition('name', $condition, '=')
    ->execute();

  drupal_set_message(t($message), 'status', FALSE);
}

/**
 * Function to get all the clan invites of current clan
 */
function _get_clan_invites() {
  global $user;
  $invites_array = array(); // Create array that hols clan invites

  $invites = db_select('clans_invite', 'i')
    ->fields('i')
    ->condition('cid', $user->cid ,'=')
    ->condition('type', 1, '=') //0 = Clan invites Player, 1 = Player invites Clan.
    ->execute();

  // Check if user has clan invites
  if ($invites->rowCount() != 0) {
    $invites = $invites->fetchAllAssoc('uid'); // Get all clan invites bij user id

    // Foreach clan invite get the user name and id
    foreach ($invites as $i) {
      $name = db_select('users', 'u')
        ->fields('u')
        ->condition('uid', $i->uid ,'=')
        ->execute()
        ->fetchAssoc();

      $invites_array[$name['uid']] = $name['name']; // Set the clan invite in the invites array
    }

  }

  return $invites_array;
}
