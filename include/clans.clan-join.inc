<?php

/**
 * Page callback for clan/%/join
 */
function clans_join($clan) {
  $clan_info = NULL;
  if(is_numeric($clan)) { // If $clan is a number get clan info by ID
    // Query to get clan info
    $clan_info = db_select('clans', 'c')
      ->fields('c')
      ->condition('cid', $clan,'=')
      ->execute();

  } else { // If $clan is a strung get clan info by Name
    // Query to get clan info
    $clan_info = db_select('clans', 'c')
      ->fields('c')
      ->condition('name', $clan,'=')
      ->execute();
  }

  // Check if clan exists
  if ($clan_info->rowCount() != 0) { // If clan exists get all the info and bind it to $clan_info
    $clan_info = $clan_info->fetchAssoc();
  } else { // If clan doesn't exists create an error and sens user to their own profile
    drupal_set_message(t('Clan doesn\'t exists'), 'error', FALSE);
    drupal_goto('user');
  }

  // If clan is private (0 = private, 1 = open)
  if ($clan_info['private']) { // If clan is open send a invite to the clan
    // But first we need to check if the clan already has there max Clan Players
    // Create Query that selects all clan members of the clan
    $members = db_select('users', 'u')
      ->fields('u')
      ->condition('cid', $clan_info['cid'], '=')
      ->condition('rank', 1, '=')
      ->execute()
      ->rowCount();

    if ($clan_info['members'] == $members) { // If clan has already there max clan members create an error
      drupal_set_message(t('Clan already have there maximum members'), 'error', FALSE);
      drupal_goto('clan/'.$clan);
    } else {
      global $user;
      // Check if user already has submitted to join this clan
      $invite = db_select('clans_invite', 'i')
        ->fields('i')
        ->condition('cid', $clan_info['cid'], '=')
        ->condition('uid', $user->uid, '=')
        ->execute()
        ->rowCount();

      if ($invite != 0) { // User already has submitted to join this clan, so we need to let him know that
        drupal_set_message(t('Join already submitted to join this clan'), 'error', FALSE);
        drupal_goto('clan/'.$clan);
      } else {
        // Create the invite
        $query = db_insert('clans_invite')
          ->fields(array(
            'type' => 1, //0 = Clan invites Player, 1 = Player invites Clan.
            'uid' => $user->uid,
            'cid' => $clan_info['cid'],
          ))
          ->execute();

        // Clan join has been submitted with succes, create a message and send current page viewer to clan profile
        drupal_set_message(t('Your clan invite has been send.'), 'status', FALSE);
        drupal_goto('clan/'.$clan);
      }
    }
  } else { // Clan is Private
    // Create message that let the user know the clan is private
    drupal_set_message(t('Sorry, this clan is private.'), 'warning', FALSE);
    drupal_goto('clan/'.$clan);
  }
}
