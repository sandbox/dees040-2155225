<div class='clan <?php print $name; ?>'>
	<div class='clan-info'>
    <table>
      <tr>
        <td>
          Tag
        </td>
        <td>
          <?php print $tag; ?>
        </td>
      </tr>
      <tr>
        <td>
          Score
        </td>
        <td>
          <?php print $score; ?>
        </td>
      </tr>
      <tr>
        <td>
          Date Created
        </td>
        <td>
          <?php print date("M j Y", $created_at); ?>
        </td>
      </tr>
      <tr>
        <td>
          Creator
        </td>
        <td>
          <?php print $creator; ?>
        </td>
      </tr>
    </table>
    <?php print $description; ?><br /><br />
    <?php print $link; ?>
  </div>
  <div class='clan-members'>
    <div class='items'>
      <div class='item-head'>
        <nav class='leaders'>
          <ul>
            <li class='head'><h3>Leaders</h3></li>
            <?php
              foreach ($leaders as $i) {
                print "<li class='item'><a href='".$link_u.$i."'>".$i."</a></li>";
              }
            ?>
          </ul>
        </nav>
      </div>
      <div class='item-head'>
        <nav class='admins'>
          <ul>
            <li class='head'><h3>Admins</h3></li>
            <?php
              foreach ($admins as $i) {
                print "<li class='item'><a href='".$link_u.$i."'>".$i."</a></li>";
              }
            ?>
          </ul>
        </nav>
      </div>
      <div class='item-head'>
        <nav class='members'>
          <ul>
            <li class='head'><h3>Members</h3></li>
            <?php
              foreach ($members as $i) {
                print "<li class='item'><a href='".$link_u.$i."''>".$i."</a></li>";
              }
            ?>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>
